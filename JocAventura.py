#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' 
 Institut Escola del Treball de Barcelona 
 Administració de Sistemes Informàtics en Xarxa 
 Curs 2023-24 
 Autor: Xavier Garrido 
 Data: 02-04-2024
 
 Descripció: 
 L'objectiu d'aquesta pràctica és treballar el disseny 
 modular implementant un joc en Python que carregui les 
 dades del joc en diccionaris i presenti a l'usuari un 
 menú que li permeti anar explorant el joc de manera 
 interactiva.
''' 
import sys
import random
from time import sleep
from os import path
from scripts.console_input import userInputCheck
from scripts.console_output import textToPrint
from scripts.game_header import showTitle
from scripts.package_loader import openPackages
from scripts.win_the_game import youWinTheGame
from scripts.combats import combats
from scripts.user_choose_location import userChooseLocation

DEFAULT_COINS = 20
DEFAULT_ARMORY = {'name': 'hands', "Attack_bonus": 5,}
PACKAGE_DICTIONARIS = {}
inventory = {'coins': DEFAULT_COINS,'items': [DEFAULT_ARMORY]}


    
def shoping(shopType):
    '''
    shoping The function to show and buy items

    Args:
        shopType (String): the name of the shop
    '''    
    i = 1
    textToPrint("\nThis is what is available in the store")
    for itemKey, itemDictionary in PACKAGE_DICTIONARIS['item'].items():    
        
        if itemDictionary['Type'] == shopType.split('_')[0]:
            num = ("{:02d}".format(i))
            i += 1
            textToPrint(f"\t{num}: {itemKey}")
            for itemKeyValue,itemValue in itemDictionary.items():  
                textToPrint(f"\t    {itemKeyValue}: {itemValue}", sleepTime=0.02)
    
    textToPrint(f"Which item do you want to buy? You have {inventory['coins']} coins.")
    userItem = userInputCheck() 
    if len(userItem) == 0:
        textToPrint("\nLeaving the store...") 
    elif userItem in PACKAGE_DICTIONARIS['item']:
        if inventory['coins'] >= PACKAGE_DICTIONARIS['item'][userItem]['Price']:
            textToPrint(f"You have bought {userItem}")
            itemToAdd = PACKAGE_DICTIONARIS['item'][userItem]
            itemToAdd['name'] = userItem
            inventory['items'].append(itemToAdd)
            inventory['coins'] = inventory['coins'] - PACKAGE_DICTIONARIS['item'][userItem]['Price']
        else:
            textToPrint("You don't have enough coins")
        textToPrint("You want to buy anything else? (yes/no):")
        userInput = userInputCheck()
        if userInput == 'yes':
            shoping(shopType)
        else:
            textToPrint("\nLeaving the store...")        
    else:
        textToPrint("\nYou have to choose one of the showed items.")
        shoping(shopType)


def events(locationDictionary):
    '''
    events: This function splits the locations into safe or combat types.

    Args:
        locationDictionary (dictionary): all the information of the location

    Returns:
        bool: if the locations are clear of monsters, return true
    '''    
    if 'encounters' in locationDictionary:
        randomNumber = random.randint(0,len(locationDictionary['encounters']))
        character = locationDictionary['encounters'][randomNumber]
        textToPrint("Wow, whats that sond? \nDid you hear that \nIt's a " + character)
        exploredLocation = combats(character)
        return exploredLocation
        
    if 'shops' in locationDictionary:
        i = 0

        textToPrint("\nChoose which store you want to go to, otherwise leave it blank:")
        for shop in locationDictionary['shops']:
            sleep(0.5)
            num = ("{:02d}".format(i))
            textToPrint(f"\t{num}: " + shop)
            i += 1
        sleep(0.5)
        textToPrint("Write were you want to go:")
        shopToGo = userInputCheck()
        if len(shopToGo) == 0:
            return 
        for shop in locationDictionary['shops']:
            if shop == 'weapon_shop': # Esta es la lina a cambiar si se quieren habilitar mas tiendas
                shoping(shopToGo)
                events(locationDictionary)
                travel(userChooseLocation())
                return 
        
        # Error 
        textToPrint("You have to chose one of the shops.")

        events(locationDictionary)

 
def travel(choosedLocation):
    '''
    travel: To pass from the menu to a location

    Args:
        choosedLocation (String):The name of the location
    '''    
    # Before go to location
    sleep(1)
    textToPrint(f"\nYou are now in {choosedLocation}")
    sleep(0.5)
    locationDictionary = PACKAGE_DICTIONARIS['location'][choosedLocation]

    eventDone = events(locationDictionary)

    # After go to location
    if (eventDone == True) and (PACKAGE_DICTIONARIS['location'][choosedLocation]['explored'] == False ): 
        PACKAGE_DICTIONARIS['location'][choosedLocation]['explored'] = True
    print()

def menu():
    '''
    menu: This function is the core of the game and is in a loop from the moment it is called until the end of the game.
    '''    
    while True:
        choosedLocation = userChooseLocation()
        travel(choosedLocation)
        allLocationsDone = True
        for locations in PACKAGE_DICTIONARIS['location']:
            if allLocationsDone == False:
                break
            elif PACKAGE_DICTIONARIS['location'][locations]['explored'] == False:
                allLocationsDone = False
        
        if allLocationsDone:
            youWinTheGame()

def main():
    '''
    main: Main function
    '''    
    global PACKAGE_DICTIONARIS
    packagePath = path.dirname(__file__) + '/packages/'
    showTitle()
    PACKAGE_DICTIONARIS = openPackages(packagePath)
    menu()

if __name__ == '__main__':
    main()