# Juego de Aventura

## Descripción
Este juego de aventura escrito en Python te permite explorar un mundo misterioso lleno de monstruos y desafíos. Carga los datos del juego en diccionarios y te presenta un menú interactivo que te permite explorar el mundo y enfrentarte a diferentes enemigos.

## Instrucciones de Uso
1. Asegúrate de tener Python instalado en tu sistema.
2. Descarga los archivos del juego en tu computadora.
3. Abre una terminal o línea de comandos en el directorio donde guardaste los archivos del juego.
4. Ejecuta el archivo principal del juego `JocAventura.py` utilizando Python:

    ```
    python JocAventura.py
    ```

5. Sigue las instrucciones en pantalla para explorar el mundo del juego, enfrentarte a monstruos, comprar objetos y completar misiones.

## Características del Juego
- **Exploración**: Viaja a diferentes ubicaciones y descubre los secretos del mundo del juego.
- **Combates**: Enfréntate a una variedad de enemigos, cada uno con sus propias habilidades y estadísticas.
- **Comercio**: Visita tiendas para comprar nuevos objetos y mejorar tu equipo.
- **Progresión**: Completa misiones y derrota enemigos para ganar monedas y mejorar tus habilidades.

## Requisitos del Sistema
- Python 3.x
- Terminal o línea de comandos

## Créditos
- **Autor**: Xavier Garrido
- **Institución**: Institut Escola del Treball de Barcelona
- **Curso**: Administració de Sistemes Informàtics en Xarxa
- **Año**: 2023-24

¡Disfruta del juego y que la aventura comience!
