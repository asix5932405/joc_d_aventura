import sys
from .console_output import textToPrint

def youWinTheGame():
    '''
    youWinTheGame: The last function that will close the game
    '''    
    textToPrint("Finally this village can rest in peace. All surroundings are free of monsters. \nThey have offered you a house and a lot of money. \nBut you've gotten a letter from Cartoon Town and you know that it's the call of duty...")
    sys.exit()