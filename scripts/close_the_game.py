#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from .console_output import textToPrint

def exitGame():
    '''
    exitGame: This function makes sure that you want to exit the game, and makes it
    '''    
    textToPrint('Are you sure about to leave the game, you will lost all progres:')
    answerToLeaveTheGame = input("(yes/no): ")

    if answerToLeaveTheGame == 'yes': 
        sys.exit()
    elif answerToLeaveTheGame == 'no':
        return
    else:
        textToPrint('Choose one of the only 2 options: yes or not')
        exitGame()
