#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from os import path,listdir

def openPackages(packagePath):
    '''
    openPackages: This function loads each JSON file in the packages directory and puts them all in a dictionary

    Args:
        packagePath (String): The path to find packages directory

    Returns:
        Dictionary: The dictionary with all JSON dictionaries loads 
    '''    

    packageDictionary = {}

    for package in listdir(packagePath):
        packageJSON = open(packagePath+package)
        packageDATA = json.load(packageJSON)
        packageJSON.close()

        packageDictionary[f"{package.split('.')[0]}"] = packageDATA 

    return packageDictionary
