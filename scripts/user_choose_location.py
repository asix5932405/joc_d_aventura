from console_output import textToPrint
from JocAventura import PACKAGE_DICTIONARIS
from console_input import userInputCheck

def userChooseLocation():
    '''
    userChooseLocation The functions that the game uses to know where the player wants to go

    Returns:
        String: the name of the location
    '''    
    # Show localitations
    textToPrint("You have this locations to choose:")
    i = 1
    for location in PACKAGE_DICTIONARIS['location']:
        num = ("{:02d}".format(i))
        textToPrint(f"\t{num}: " + location, end=' ')
        if PACKAGE_DICTIONARIS['location'][location]['explored']:
            textToPrint("(Done)")
        else:
            textToPrint("(To Explore)")
        i += 1
    textToPrint("Write were you want to go:")
    inputLocalitation = userInputCheck()

    # Choose localitation
    for location in PACKAGE_DICTIONARIS['location']:
        if inputLocalitation == location:
            return inputLocalitation
    
    # Error 
    textToPrint("\nYou have to choose one of the locations showed before\n")
    userChooseLocation()