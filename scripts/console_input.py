#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .close_the_game import exitGame

def userInputCheck(combat=False):
    '''
    userInputCheck: This function examines every input and takes the necessary actions

    Args:
        combat (bool, optional): If the player is in combat. Defaults to False.

    Returns:
        String: The user input
    '''    
    userInput = input()
    match userInput:
        case 'kill':
            if combat == True:
                return "kill"
        case 'exit'|'EXIT':
            exitGame()
        case _:
            return userInput
