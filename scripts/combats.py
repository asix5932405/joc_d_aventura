from console_output import textToPrint
from JocAventura import PACKAGE_DICTIONARIS,inventory 
from console_input import userInputCheck

def showItems():
    '''
    showItems This function show all the items in the inventory
    '''    
    i = 1
    for item in inventory['items']:
        num = ("{:02d}".format(i))
        textToPrint(f"\t{num}: {item['name']}")
        for itemKey,itemValue in item.items():
            if itemKey != 'name':
                textToPrint(f"\t    {itemKey}: {itemValue}",sleepTime=0.005)
        i += 1

def combats(character):
    '''
    combats the function of the game combat

    Args:
        character (String): The name of the monster

    Returns:
        Bool: to know if the player won or not, the combat
    '''    
    enemyStats = PACKAGE_DICTIONARIS['character'][character]
    chatacterStats = PACKAGE_DICTIONARIS['character']['player']
    enemyHealthPoints = enemyStats['health']
    mainCharacterHealthPoints = chatacterStats['health']
    
    while enemyHealthPoints > 0 and mainCharacterHealthPoints > 0:
        textToPrint(f"\nIt have {enemyHealthPoints} points of health, and you have {mainCharacterHealthPoints} points. \nYou have those items")
        showItems()
        
        textToPrint(f"Which item will you choose: ")
        inputChoosed = userInputCheck()

        if inputChoosed == "kill":
            enemyHealthPoints -= 300
        for itemChosed in inventory['items']:
            if itemChosed['name'] == inputChoosed:
                enemyHealthPoints -= itemChosed['Attack_bonus']
        
        
        mainCharacterHealthPoints -= enemyStats['attack']