
import sys
from time import sleep
from os import get_terminal_size

def getTerminalSize():
    '''
    getTerminalSize: To get the terminal size

    Returns:
        int: The terminal size
    '''    
    return get_terminal_size()[0]

def headerLine():
    '''
    headerLine: It shows a line from left to right
    '''    
    for i in range(0,getTerminalSize):
        sys.stdout.write('-')
        sys.stdout.flush()
        sleep(0.005)
    print()

def textToPrint(text, isCenter=False, sleepTime=0.02, skipable=False, end='\n'):
    '''
    textToPrint: This function makes a print, but with a sleep time  after every character

    Args:
        text (String): Text to print
        isCenter (bool, optional): If the text needs to be in the center of the terminal. Defaults to False.
        sleepTime (float, optional): The amount of time after each character. Defaults to 0.02.
        skipable (bool, optional): If the texts it's skipable. Defaults to False. For the moment is not in use
        end (str, optional): If you want to change how to end the text. Defaults to '\n'.
    '''    
    if isCenter:
        amountOfSpaces = ((getTerminalSize() - len(text)) // 2)
        print(amountOfSpaces*(' '),end='')

    for char in text:       
        sys.stdout.write(char)
        sys.stdout.flush()
        sleep(sleepTime)
    print(end=end)