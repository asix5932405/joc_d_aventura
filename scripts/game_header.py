
import sys
from time import sleep
from os import system,name
from .console_output import textToPrint,headerLine


def showTitle():
    '''
    showTitle: This function shows the game title
    '''    

    title = "JOC D'AVENTURA"

    # Clear console
    system('cls' if name == 'nt' else 'clear')

    # Terminal size
    print()
    headerLine()
    textToPrint(title, isCenter=True)
    headerLine()
    textToPrint("\nHello Jack, they called you again from Hill Village to release the different monster locations for them. Once you finish all of them, you will finally obtain your desired retirement. Good luck.")